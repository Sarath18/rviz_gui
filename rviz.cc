#include <ignition/common/Console.hh>

#ifndef Q_MOC_RUN
  #include <ignition/gui/Application.hh>
  #include <ignition/gui/MainWindow.hh>
  #include <ignition/gui/qt.h>
  #include "rviz.hh"
#endif

int main(int argc, char **argv) {
  ros::init(argc, argv, "rviz_gui");

  ros::AsyncSpinner spinner(4);
  spinner.start();

  ignition::common::Console::SetVerbosity(4);

  ignition::gui::Application app(argc, argv);

  app.LoadPlugin("Scene3D");
  app.LoadPlugin("Publisher");

  ignition::gui::RViz rviz;
  auto context = new QQmlContext(app.Engine()->rootContext());
  context->setContextProperty("RViz", &rviz);

  QQmlComponent component(app.Engine(), ":/RViz/rviz.qml");
  auto item = qobject_cast<QQuickItem *>(component.create(context));
  if(!item) {
    ignerr << "Failed to initialize" << std::endl;
    return 1;
  }

  QQmlEngine::setObjectOwnership(item, QQmlEngine::CppOwnership);

  auto win = app.findChild<ignition::gui::MainWindow *>()->QuickWindow();
  auto displayType = win->findChild<QQuickItem *>("sideDrawer");

  item->setParentItem(displayType);
  item->setParent(app.Engine());

  app.exec();

  return 0;
}
