#ifndef POINT_HH_
#define POINT_HH_

#ifndef Q_MOC_RUN
  #include <ignition/gui/qt.h>
  #include <ignition/gui/Plugin.hh>
#endif
#include <ignition/rendering.hh>

#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>

namespace ignition {
  namespace gui {
    class PointViz : public Plugin {
      Q_OBJECT
      private:
      int count;
      float scale;
      ros::NodeHandle nh;
      rendering::RenderEngine *engine;
      rendering::ScenePtr scene;
      rendering::GeometryPtr box_geometry, cylinder_geometry, sphere_geometry, cone_geometry, plane_geometry;
      rendering:: MaterialPtr mat;
      ros::Subscriber point_subscriber;
      std::string point_topic;

      public: PointViz();
      public: virtual ~PointViz();
      protected slots: void SetTopic(const QString& topic);
      protected slots: void SetSize(const QString& topic);
      protected slots: void SetColor(const QColor& color);
      public: void point_callback(const geometry_msgs::PointStampedConstPtr &);
      public: virtual void LoadConfig(const tinyxml2::XMLElement *_pluginElem);
    };
  }
}

#endif