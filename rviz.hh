#ifndef RVIZ_HH_
#define RVIZ_HH_

#include <iostream>

#ifndef Q_MOC_RUN
  #include <ignition/gui/qt.h>
  #include <ignition/gui/Application.hh>
#endif

#include <ros/ros.h>

namespace ignition {
  namespace gui {
    class RViz : public QObject {
      Q_OBJECT
      public: Q_INVOKABLE void addGrid3D() const {
        App()->LoadPlugin("Grid3D");
      }

      public: Q_INVOKABLE void addPointVisualization() const {
        App()->LoadPlugin("PointViz");
      }
    };
  }
}

#endif