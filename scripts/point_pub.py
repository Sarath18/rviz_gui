#!/usr/bin/env python
import sys
import rospy
from math import sin
from geometry_msgs.msg import PointStamped

def talker():
    pub = rospy.Publisher(str(sys.argv[1]), PointStamped, queue_size=10)
    rospy.init_node('test_node', anonymous=True)
    rate = rospy.Rate(1)
    
    x = 0
    flag = 1

    if str(sys.argv[1]) == "point":
    	flag = -1

    rospy.loginfo("Publishing Points")

    for i in range(5):
        ps = PointStamped()
        ps.point.x = i + flag
        ps.point.y = i * flag
        ps.point.z = i
        pub.publish(ps)
        rate.sleep()

    rospy.loginfo("Done")

    pub.publish(ps)

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass