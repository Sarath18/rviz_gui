#include <iostream>
#include <ignition/plugin/Register.hh>
#include <ignition/gui/Application.hh>
#include "PointViz.hh"
#include <ignition/rendering.hh>

using namespace ignition;
using namespace gui;

PointViz::PointViz() : Plugin(), count(0), scale(1.0), point_topic("point"){
  this->engine = rendering::engine("ogre");
  this->scene = this->engine->SceneByName("scene");

  rendering::PointLightPtr lightSource = scene->CreatePointLight();
  lightSource->SetLocalPosition(0,0,5);

  rendering::VisualPtr root = scene->RootVisual();
  root->AddChild(lightSource);

  rendering::MaterialPtr green = scene->CreateMaterial("green");
  green->SetAmbient(ignition::math::Color(0.0,1.0,0.0));
  green->SetDiffuse(ignition::math::Color(0.0,1.0,0.0));

  rendering::MaterialPtr red = scene->CreateMaterial("red");
  red->SetAmbient(ignition::math::Color(1.0,0.0,0.0));
  red->SetDiffuse(ignition::math::Color(1.0,0.0,0.0));

  mat = scene->Material("red");

  cylinder_geometry =  scene->CreateCylinder();
  cone_geometry = scene->CreateCone();
  sphere_geometry = scene->CreateSphere();
  plane_geometry = scene->CreatePlane();
  box_geometry = scene->CreateBox();

  point_subscriber = nh.subscribe(point_topic, 1, &PointViz::point_callback, this);
}

PointViz::~PointViz() {

}

void PointViz::LoadConfig(const tinyxml2::XMLElement */*_pluginElem*/)
{
  if (this->title.empty())
    this->title = "PointStamped";
}

void PointViz::point_callback(const geometry_msgs::PointStampedConstPtr& msg) {
  rendering::VisualPtr root = scene->RootVisual();

  ROS_DEBUG("[ Point Received ]");
  rendering::VisualPtr box = scene->CreateVisual("PointViz" + std::to_string(count));
  box->AddGeometry(scene->CreateBox());
  box->SetMaterial(mat);
  box->SetLocalScale(scale);

  box->SetLocalPosition(msg->point.x, msg->point.y, msg->point.z);

  root->AddChild(box);
  count++;
}

void PointViz::SetTopic(const QString& topic) {
  point_topic = topic.toStdString();
  if(point_topic.empty()) return;
  ROS_DEBUG("[ Topic changed to %s ]", point_topic.c_str());
  point_subscriber.shutdown();
  point_subscriber = nh.subscribe(point_topic, 1, &PointViz::point_callback, this);
};

void PointViz::SetSize(const QString& topic) {
  try {
    scale = std::stof(topic.toStdString());
    for(int i=0;i<count;i++) {
      rendering::VisualPtr visual = scene->VisualByName("PointViz" + std::to_string(i));
      visual->SetLocalScale(scale);
    }
  } catch(...) {
    ROS_ERROR("ERROR SETTING SCALE");
  }
}

void PointViz::SetColor(const QColor& color) {
  mat->SetAmbient(color.redF(), color.greenF(), color.blueF());
  mat->SetDiffuse(color.redF(), color.greenF(), color.blueF());

  for(int i=0;i<count;i++) {
    rendering::VisualPtr visual = scene->VisualByName("PointViz" + std::to_string(i));
    visual->SetMaterial(mat);

  }
}

IGNITION_ADD_PLUGIN(ignition::gui::PointViz,
                    ignition::gui::Plugin);