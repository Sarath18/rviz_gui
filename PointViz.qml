import QtQuick 2.0

import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.0

Item {
    id: pluginContainer
    height: 150
    width: 300

    TextField {
        x: 10
        y: 10
        id: topic
        width: 150
        placeholderText: qsTr("Topic Name")
        text: "/point"
    }

    Button {
        width: 50
        text: qsTr("Set")
        x: 180
        y: 10
        onClicked: { PointViz.SetTopic(topic.text) }
    }

    TextField {
        x: 10
        y: 55
        id: scale
        width: 150
        placeholderText: qsTr("Scale")
        text: "1.0"
    }

    Button {
        width: 50
        text: qsTr("Set")
        x: 180
        y: 55
        onClicked: { PointViz.SetSize(scale.text) }
    }

    Label {
        text: "Color"
        x: 10
        y: 110
        font.pixelSize: 14
    }

    Button {
        x: 55
        y: 105
        width: 50
        height: 30
        onClicked: colorDialog.open()
        background: Rectangle {
            id: buttonBg
            color: "red"
        }
    }

    ColorDialog {
        id: colorDialog
        title: "Please choose a color"
        onAccepted: {
            buttonBg.color = colorDialog.color
            PointViz.SetColor(colorDialog.color)
        }
        onRejected: {
            console.log("Canceled")
        }
        Component.onCompleted: visible = false
    }
}